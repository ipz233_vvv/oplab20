#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
/*
�������� ��� ������ x[a], y[b]. ������� ��� ������, �� ������
������:
a. �������� ���� ���������� ������
b. �� ������ ��������
*/
int main() {
	int na, nb;
	int *ma, *mb;
	printf("Enter size of array a: \n");
	scanf("%d", &na);
	ma = (int*)malloc(na * sizeof(int));
	printf("Enter elements of array a:\n");
	for (int i = 0; i < na; i++) {
		scanf("%d", ma + i);
	}
	printf("Enter size of array b: \n");
	scanf("%d", &nb);
	mb = (int*)malloc(nb * sizeof(int));
	printf("Enter elements of array b:\n");
	for (int i = 0; i < nb; i++) {
		scanf("%d", mb + i);
	}
	int *mab = (int*) malloc((na + nb) * sizeof(int));
	printf("Combined array:\n");
	for (int i = 0; i < na + nb; i++) {
		*(mab + i) = i < na ? *(ma + i) : *(mb - na + i);
		printf("%d ", *(mab + i));
	}
	int* mc = (int*)malloc((na+nb)*sizeof(int));
	int nc = 0;
	printf("\nCommon elements between arrays a and b:\n");
	for (int i = 0; i < na; i++) {
		for (int j = 0; j < nb; j++) {
			if (*(ma + i) == *(mb + j)) {
				*(mc+nc) = *(ma + i);
				printf("%d ", *(mc + nc));
				nc++;
				*(ma + i) = INT_MIN;
				*(mb + j) = INT_MIN;
			}
		}
	}
	mc = (int*)realloc(mc, nc * sizeof(int));
	printf("\n");
	free(mc);
	free(mab);
	free(ma);
	free(mb);
}
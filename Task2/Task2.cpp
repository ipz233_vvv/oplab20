#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
int main() {
	int n, k;
	printf("Enter row count: ");
	scanf("%d", &n);
	printf("Enter column count: ");
	scanf("%d", &k);
	int** m = (int**)malloc(n * sizeof(int*));
	for (int i = 0; i < n; i++)
		*(m + i) = (int*)malloc(k * sizeof(int));
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < k; j++) {
			*(*(m + i) + j) = rand() % (n * k % 100);
			printf("%2d ", *(*(m + i) + j));
		}
		printf("\n");
	}
	int insert;
	printf("Enter a line number to insert a new row: ");
	scanf("%d", &insert);
	n++;
	m = (int**)realloc(m, n * sizeof(int*));
	*(m + n - 1) = (int*)malloc(k * sizeof(int));
	for (int i = n - 1; i > insert; i--)
		for (int j = 0; j < k; j++)
			*(*(m + i) + j) = *(*(m + i - 1) + j);
	printf("Enter new row elements:\n");
	for (int j = 0; j < k; j++)
		scanf("%d", (*(m + insert) + j));
	int min_col = 0, min_val = *(*(m + 0) + 0);
	for (int j = 0; j < k; j++)
		if (*(*(m + 0) + j) < min_val) {
			min_val = *(*(m + 0) + j);
			min_col = j;
		}
	for (int i = 0; i < n; i++) {
		for (int j = min_col; j < k - 1; j++)
			*(*(m + i) + j) = *(*(m + i) + j + 1);
	}
	k--;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < k; j++)
			printf("%2d ", *(*(m + i) + j));
		printf("\n");
	}
	for (int i = 0; i < n; i++)
		free(*(m + i));
	free(m);
	return 0;
}
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#define SIZE 4
/*
�������� 1. �������� �������� � ������������� ���������.
������� ������ �������� ������� 4�4 , �� ����������� ����
������� �������.
*/
int main() {
	int** p = (int**)malloc(SIZE * sizeof(int*));
	for (int i = 0; i < SIZE; i++) {
		*(p + i) = (int*)malloc(SIZE * sizeof(int));
		for (int j = 0; j < SIZE; j++) {
			*(*(p + i) + j) = rand() % 10;
			if (j > i)
				printf("%d ", *(*(p + i) + j));
			else
				printf("  ");
		}
		printf("\n");
	}

	for (int i = 0; i < SIZE; i++) {
		free(*(p + i));
	}
	free(p);
}